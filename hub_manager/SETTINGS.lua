-- spawn point
hub.settings.hub_spawn_point = vector.new(6, 33, 170)

-- hotbar additional items
hub.settings.hotbar_items = {
  "",
  "",
  "magic_compass:compass",
  "",
  "skins_collectible:wardrobe",
  "",
  "serverguide:guide"
}

-- physics whilst in the lobby
hub.settings.physics = {
  speed=1.5,
  jump=1,
  gravity=1,
  sneak=true,
  sneak_glitch=true,
  new_move=false
}

-- how many arenas to display in the arenas status panel (on the right)
hub.settings.MAX_ARENAS_IN_STATUS = 4

-- max slots in the playerlist table. If there are more players, it'll say "...and much more!" on the last slot
hub.settings.plist_max_slots = 36

-- max players per column in the playerlist table. When N +1 players are reached, a new column is generated, recalculating the position of the previous slots
hub.settings.plist_p_per_column = 12
